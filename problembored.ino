// projectname fuckswitch

// readme datei mit bild
// rotapol gehta auch bei der arri, ist aber farbsticjig
// gerät nimmt per encoder die werte vom blendenring. hat nämlich den vorteil,
// das es bei allem kameras geht.

// simons resterampe. synchronisiert einen blendenzug an der kamera mit einem dmx-signal, 
// welches auf z b einnen blendenzug an eine lampe überträgt.




// d9 aka gpio2 geht auf sig dmx
#include <ESPDMX.h>
#include <ESP8266WiFi.h>
#include <espnow.h>
#include "AiEsp32RotaryEncoder.h"
#include <Wire.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiWire.h"
#define I2C_ADDRESS 0x3C


#define ROTARY_ENCODER_A_PIN D5
#define ROTARY_ENCODER_B_PIN D6
#define ROTARY_ENCODER_STEPS 2
AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, -1, ROTARY_ENCODER_STEPS);

#define CHBut D2. // channel button - waehlt dmx-channel aus
#define ValBut D10 // value - button -  versteh ich noch nicht
//#define LowBut D1 

// optical encoder auf extra gerät


void IRAM_ATTR readEncoderISR()
{
    rotaryEncoder.readEncoder_ISR();
}

// ESP NOW - struktuzr für datenuebermittlung
// Structure example to receive data
// Must match the sender structure

typedef struct struct_message {. // hier mal durchdokumentieren
    char a[32];  // 
    int b;  // hier einfach dranschreiben was die 
    int c;  // 
    int d;  //
    int e;  //
} struct_message;

// Create a struct_message called myData
struct_message myData;

// espnow call back funktion bei datenemepfang
// Callback function that will be executed when data is received
void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len) {
  memcpy(&myData, incomingData, sizeof(myData));
  //Serial.print("Bytes received: ");
  //Serial.println(len);
  //Serial.print("Char: ");
  //Serial.println(myData.a);
  //Serial.print("B: ");
  //Serial.println(myData.b);
  //Serial.print("C: ");
  //Serial.println(myData.c);
  //Serial.print("D: ");
  //Serial.println(myData.d);
  //Serial.print("E: ");
  //Serial.println(myData.e);
  //Serial.println();
}

DMXESPSerial dmx;
SSD1306AsciiWire oled;


int myChannel = 0;  //verstehe ich nicht aus dem variablen namen nicht eindeutigere namen vergeben
int dimPosition1 = 0;
int dimPosition2 = 0;
int Channel = 0;
long Rotawert1 = 0;  // besserneamenswahl rot-a-wert hab ich gelöesen
long Rotawert2 = 0;
float St = 0.00;  // steigung
int dSt = 0; // ist *0.001 das gleiche wie durch 100 teilen?
int ro = 0; 
int myRota = 0;
int dimPo = 0;
int Dim = 0;
//int Rota = 0;
byte DIMON = 0;
int ValST = 0;


void setup()
{
    Serial.begin(115200);
    rotaryEncoder.begin();
    rotaryEncoder.setup(readEncoderISR);
    rotaryEncoder.setBoundaries(0, 1026, true);  // sets boundaries for encoder value, 1024 + 2 for dmxchannel hack (512 wird nict erreicht bei 1024)
    rotaryEncoder.setAcceleration(1);

    Wire.begin();
    Wire.setClock(400000L);
     oled.begin(&Adafruit128x64, I2C_ADDRESS);

          // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    oled.println(" Verbunden");
    return;
  }
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
  esp_now_register_recv_cb(OnDataRecv);

  dmx.init();
  delay(200);

}


void loop()
{
  oled.setFont(System5x7);
  oled.clear();
  oled.println(" Lucifer");
  oled.print(" DMX CH ");  
  oled.println(myChannel);   // dmxkanal der gesteuert wird 
  oled.print(" IN ");
  oled.println(dimPosition1); // anfangsdimposition von dem ablauf 
  oled.print("  RotaI "); // rotationswert INNEN
  oled.println(Rotawert1);   
  oled.print(" OUT ");
  oled.println(dimPosition2); // endposition des ablaufes [dmx-wert]
  oled.print(" RotaO ");  
  oled.println(Rotawert2); //rotationswert [stellung des encoders]
  oled.print(" STEIG ");
  oled.println(dSt);  // dimmersteigung

  int16_t currentValue = rotaryEncoder.readEncoder();  // current_encoder_reading oder so waere ein eindeutigerer name

    //dSt = dimPo * (1/ro); 
    //dSt = dimPo / ro;

  
   if (digitalRead(CHBut) == HIGH) {
        myChannel= currentValue/2; // 1/2ierung um aus 1024 encoderwerten [0..512] zu machen
        oled.clear();
        oled.print("  DMX Sel  ");
        oled.println(currentValue/2);
   }


 int buttonState = digitalRead(ValBut);
  if (buttonState == HIGH) {
     ValST = ValST + 1;
  } 
  else {
     ValST = ValST;
  }

   if (ValST>5) {
    ValST = 0;}
    else {
     ValST = ValST;
   }

  
 Serial.println(ValST);

    switch (ValST) { 
      case 1:   
    oled.println(" CAL IN ");
    dimPosition1 = currentValue/4;
    
    dmx.write(myChannel, currentValue/4);
    dmx.update();
        break;

      case 2: 
    oled.println(" CAL IN SET");
      dimPosition1 = currentValue/4;
      Rotawert1 = myData.b;

    dmx.write(myChannel, dimPosition1);
    dmx.update();
        break;

      case 3:
    oled.println(" CAL OUT ");
    dmx.write(myChannel, currentValue/4);
    dmx.update();

      case 4:
    oled.println(" CAL OUT SET");  
    dimPosition2 = currentValue/4;
    Rotawert2 = myData.b;

   dimPo = dimPosition1 - dimPosition2;
   ro = Rotawert1 - Rotawert2;
    
    dmx.write(myChannel, dimPosition2);
    dmx.update();
    break;

    case 5: 
    oled.println(" CAL EXIT "); 

        break;
    }

  //dSt = (dimPo*100)/(ro*100);  //das hier ist das problem hier stürtzt der esp immer ab


    DIMON = dSt * myData.b;
    
   //Serial.println(rotaryEncoder.readEncoder());
   //Serial.println(myData.b);
   Serial.print("DimPo");
   Serial.println(dimPo);
   Serial.print("Rota");
   Serial.println(ro);
   Serial.print("DIMSteig");
   Serial.println(dSt);
   Serial.print("IN");
   Serial.println(dimPosition1);
   Serial.print("OUT");
   Serial.println(dimPosition2);
 //  Serial.print("CH");
 //  Serial.println(myChannel);  
   Serial.print("Dimon");
   Serial.println(DIMON);
   
   dmx.write(myChannel, DIMON);
   dmx.update();
    delay(50);
}
